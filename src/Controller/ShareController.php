<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Posts Controller
 *
 * @property \App\Model\Table\PostsTable $Posts
 * @method \App\Model\Entity\Post[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ShareController extends AppController
{
    public function initialize(): void
    {
        parent::initialize();
        $this->viewBuilder()->setLayout('app');
        $this->loadModel('Posts');
        $this->loadModel('Users');
        $this->loadModel('SharePosts');
        $this->loadModel('Likes');
        $this->loadModel('Comments');
        $this->loadModel('Followers');
    }

    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        $this->Authentication->addUnauthenticatedActions(['login']);
        $this->Authentication->addUnauthenticatedActions(['login', 'add']);
    }

    public function share()
    {
        $identity = $this->request->getAttribute('authentication')->getIdentity();
        $share = $this->SharePosts->newEmptyEntity();
        if ($this->request->is('ajax')) {
            $share = $this->SharePosts->patchEntity($share, $this->request->getData());
            $share->user_id = $identity->id;
            if ($this->SharePosts->save($share)) {
                $this->Flash->success(__('You share this posts.'));
            }
        }

        return $this->redirect(['controller' => 'Profile', 'action' => 'profile', $share->user_id]);
        $this->set(compact('share', 'users'));
    }
}
