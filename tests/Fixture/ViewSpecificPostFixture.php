<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ViewSpecificPostFixture
 */
class ViewSpecificPostFixture extends TestFixture
{
    /**
     * Table name
     *
     * @var string
     */
    public $table = 'view_specific_post';
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'reactions_id' => 1,
                'title' => 'Lorem ipsum dolor sit amet',
                'profile_pic' => 'Lorem ipsum dolor sit amet',
                'fullname' => 'Lorem ipsum dolor sit amet',
                'email' => 'Lorem ipsum dolor sit amet',
                'id' => 1,
                'user_id' => 1,
                'description' => 'Lorem ipsum dolor sit amet',
                'file_name' => 'Lorem ipsum dolor sit amet',
                'created' => '2022-01-08 13:59:39',
                'modified' => '2022-01-08 13:59:39',
            ],
        ];
        parent::init();
    }
}
