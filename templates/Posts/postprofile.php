<br>
  <?php
    echo $this->Form->create(null,['type' => 'get']);
    echo $this->Form->control('key',['label' => 'Search', 'value' => $this->request->getQuery('key'),'class' => 'form-control me-sm-2']);
    echo  $this->Form->submit(__('Search'),['class' => 'btn btn-primary']);
    echo $this->Form->end();
  ?>
  <?= $this->Html->link(__('New Post'), ['action' => 'add'], ['class' => 'button float-right']) ?>
  <div class="off-canvas-wrapper">
      <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
        <div class="off-canvas-content" data-off-canvas-content>
        <div class="container mt-4 mb-4 p-3 d-flex justify-content-center"> 
           <?php foreach ($user as $user): ?>
            <div class="card p-4" id="card-body">
              <?php if ($identity->id == $user->id): ?>
                <div class=" image d-flex flex-column justify-content-center align-items-center">
                <?php echo $this->Html->image($user->profile_pic, array(
                	'width'=>'200px',
                	'url' => array(
                             'controller' => 'Posts',
                             'action' => 'postprofile', $user->id
                    )            	
                ));?>
                 <span id="fullname" style="text-transform: capitalize;" class="name mt-3"><?=$user->fullname ?></span>
                    <div class="d-flex flex-row justify-content-center align-items-center mt-3"><button type="button" class="btn btn-primary">5 Followers </button> &nbsp&nbsp&nbsp <?= $this->Html->link(__('Following'), ['controller' => 'Followers','action' => 'following' , $user->id], ['class' => 'btn btn-primary']) ?></div>
                    <div class=" d-flex mt-2">
                  		<?php echo $this->Html->link('Edit Profile'
                  			,array('controller' => 'Users', 'action'=>'edit', $user->id)
                   			,array('class' => 'btn1 btn-dark')); 
                   		?>
                  	</div>
                   

                    <div class=" px-2 rounded mt-4 date "> <span class="join">Joined <?= $user->created ?></span> </div>
                </div>
              <?php endif ?>
              <?php  if ($identity->id != $user->id): ?>
                <input hidden="" value="<?=$user->id ?>" id="user_id" type="text" required class="form-control" placeholder="idHolder" name="user_id">
                <div class=" image d-flex flex-column justify-content-center align-items-center"> <?php echo $this->Html->image($user->profile_pic, 
                  array(
                  "alt" => "logo",
                  'url' => array(
                             'controller' => 'Posts',
                             'action' => 'postprofile', $user->id
                    )
                  )
                ); ?>

                 <span style="text-transform: capitalize;" class="name mt-3"><?=$user->fullname ?></span>
                   <div class="d-flex flex-row justify-content-center align-items-center mt-3"><button id="followBtn" type="button" class="btn btn-primary">Follow</button> &nbsp&nbsp&nbsp<button type="button" class="btn btn-primary"> 30 Followers </button>
                   </div>
                    <div class=" px-2 rounded mt-4 date "> <span class="join">Joined <?= $user->created ?></span> </div>
                </div>
              <?php endif ?>
                </div>
                 <?php endforeach ?>       
            </div>
             <?php foreach ($posts as $post): ?>   
           <div id="try" class="row medium-8 large-7 columns ">
              <div class="blog-post">
                <h3 style="text-transform: capitalize;"><?= $post->title ?> <small><?= $post->created ?></small></h3>
              <strong>
                <?= $this->Html->link(__($post->user->fullname), ['action' => 'postprofile', $post->user_id],['style' => 'text-transform: capitalize;']) ?>
              </strong>
              <p> <?= $post->description ?> </p>
                <?php echo $this->Html->image($post->file_name, 
                  array(
                  "alt" => "logo",
                  'url' => array(
                             'controller' => 'Posts',
                             'action' => 'view', $post->id
                    )
                  )
                ); ?>
              </div>
          </div><br>
       <?php endforeach ?>
        </div>
          <hr>
        </div>
      </div>
  </div>
<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="https://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.js"></script>
<script type="text/javascript">
$(function(){
  $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': csrfToken 
      }
  });
  $('#submitBtn').click(function(){
      var comment = $("#comment_id").val();
      var post_id = $("#idHolder").val(); 
      $.ajax({
        method: "POST",
        type: "JSON",
        url: "<?= $this->Url->build(['controller' => 'Posts', 'action' => 'addComment']) ?>",
        data:{ 
          comment: comment
          ,post_id: post_id
        },
        success:function(response){
                location.reload();
            }
      });
  });
  $('#followBtn').click(function(){
    var following  = $('#user_id').val();
    $.ajax({
        method: "POST",
        url: "<?= $this->Url->build(['controller' => 'Posts', 'action' => 'following']) ?>",
        data:{ 
         following: following
        },
        success:function(response){
                // location.reload();
            }
      });

  });
    $(document).ready(function()
    {
      $("#card-body").on('click','.editBtn',function()
      {
      	var fullname = $('#fullname').text().trim();
      	$('#edit_fullname').val($fullname);

      })
  });
});

</script>
