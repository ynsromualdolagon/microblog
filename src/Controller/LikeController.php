<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Posts Controller
 *
 * @property \App\Model\Table\PostsTable $Posts
 * @method \App\Model\Entity\Post[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LikeController extends AppController
{
    public function initialize(): void
    {
        parent::initialize();
        $this->viewBuilder()->setLayout('app');
        $this->loadModel('Posts');
        $this->loadModel('Users');
        $this->loadModel('SharePosts');
        $this->loadModel('Likes');
        $this->loadModel('Comments');
        $this->loadModel('Followers');
    }

    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        $this->Authentication->addUnauthenticatedActions(['login']);
        $this->Authentication->addUnauthenticatedActions(['login', 'add']);
    }

    public function like()
    {
        $identity = $this->request->getAttribute('authentication')->getIdentity();
        $reactionsTable = $this->Likes;
        $reactions = $this->Likes->newEmptyEntity();
        if ($this->request->is('ajax')) {
            $reactions = $this->Likes->patchEntity($reactions, $this->request->getData());
            // $reactions->id = $this->request->getData('post_id');
            $reactions->user_id = $identity->id;
            if ($this->Likes->save($reactions)) {
                $this->Flash->success(__('You liked this post.'));
            }
        }
        $this->set(compact('reactions', 'users'));
    }

    public function unlike($id = null)
    {
        $identity = $this->request->getAttribute('authentication')->getIdentity();
        $this->request->allowMethod(['post', 'delete']);
        $unlike = $this->Likes->get($id);
        if ($unlike->user_id = $identity->id) {
            $this->Likes->delete($unlike);
            $this->Flash->success(__('You unlike this posts'));
        }

        return $this->redirect(['controller' => 'Posts','action' => 'view', $unlike->post_id]);
    }
}
