<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Posts Controller
 *
 * @property \App\Model\Table\PostsTable $Posts
 * @method \App\Model\Entity\Post[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FollowersController extends AppController
{
    public function initialize(): void
    {
        parent::initialize();
        $this->viewBuilder()->setLayout('app');
        $this->loadModel('Posts');
        $this->loadModel('Users');
        $this->loadModel('SharePosts');
        $this->loadModel('Likes');
        $this->loadModel('Comments');
        $this->loadModel('Followers');
    }

    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        $this->Authentication->addUnauthenticatedActions(['login']);
        $this->Authentication->addUnauthenticatedActions(['login', 'add']);
    }

    public function add($user_id = null)
    {
        $identity = $this->request->getAttribute('authentication')->getIdentity();
        $followerTable = $this->Followers;
        $follower = $this->Followers->newEmptyEntity();
        if ($this->request->is('ajax')) {
            $follower = $this->Followers->patchEntity($follower, $this->request->getData());
            $follower->follower_user_id = $identity->id;
            $follower->following_user_id = $this->request->getData('following');
            if ($this->Followers->save($follower)) {
                $this->Flash->success(__('Followed Succesfully '));
            }
        }

        return $this->redirect(['controller' => 'Profile', 'action' => 'profile', $follower->follower_user_id]);
        $this->set(compact('follower', 'following', 'identity'));
    }

    public function followings()
    {
        $key = $this->request->getQuery('key');
        $identity = $this->request->getAttribute('authentication')->getIdentity();
        if ($key) {
            $following = $this->Followers->find('all')
                ->contain(['Following'])
                ->where(['fullname like' => '%' . $key . '%'])
                ->where(['follower_user_id' => $identity->id]);
        } else {
            $following = $this->Followers->find('all')
                ->contain(['Following'])
                ->where(['follower_user_id' => $identity->id]);
        }
        $users = $this->paginate($following, ['limit' => 3]);
        $this->set(compact('following', 'identity', 'users'));
    }

    public function listfollower()
    {
        $key = $this->request->getQuery('key');
        $identity = $this->request->getAttribute('authentication')->getIdentity();
        if ($key) {
            $followers = $this->Followers->find('all')
                ->contain(['Followers'])
                ->where(['fullname like' => '%' . $key . '%'])
                ->where(['follower_user_id' => $identity->id]);
        } else {
            $followers = $this->Followers->find('all')
                ->contain(['Followers'])
                ->where(['fullname like' => '%' . $key . '%'])
                ->where(['follower_user_id' => $identity->id]);
        }
        $users = $this->paginate($followers, ['limit' => 3]);
        $this->set(compact('followers', 'identity', 'users'));
    }

    public function unfollow($id = null)
    {
        $identity = $this->request->getAttribute('authentication')->getIdentity();
        $this->request->allowMethod(['post', 'delete']);
        $unfollow = $this->Followers->get($id);
        if ($unfollow->follower_user_id = $identity->id) {
            $this->Followers->delete($unfollow);
            $this->Flash->success(__('The user has been unfollowed.'));
        } else {
            $this->Flash->error(__('The user could not be unfollowed. Please, try again.'));
        }

        return $this->redirect(['controller' => 'Followers', 'action' => 'followings']);
    }
}
