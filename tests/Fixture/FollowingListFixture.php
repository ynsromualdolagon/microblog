<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * FollowingListFixture
 */
class FollowingListFixture extends TestFixture
{
    /**
     * Table name
     *
     * @var string
     */
    public $table = 'following_list';
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'fullname' => 'Lorem ipsum dolor sit amet',
                'following' => 'Lorem ipsum dolor sit amet',
                'id' => 1,
                'user_id' => 1,
                'following_user_id' => 1,
                'follower_user_id' => 1,
            ],
        ];
        parent::init();
    }
}
