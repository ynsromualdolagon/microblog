<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SharePostsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SharePostsTable Test Case
 */
class SharePostsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\SharePostsTable
     */
    protected $SharePosts;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.SharePosts',
        'app.Users',
        'app.Posts',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('SharePosts') ? [] : ['className' => SharePostsTable::class];
        $this->SharePosts = $this->getTableLocator()->get('SharePosts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->SharePosts);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\SharePostsTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\SharePostsTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
