<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * DebugFollowersFixture
 */
class DebugFollowersFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'f_id' => 1,
                'f_user_id' => 1,
                'following_id' => 1,
            ],
        ];
        parent::init();
    }
}
