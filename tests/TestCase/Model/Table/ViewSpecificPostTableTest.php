<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ViewSpecificPostTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ViewSpecificPostTable Test Case
 */
class ViewSpecificPostTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ViewSpecificPostTable
     */
    protected $ViewSpecificPost;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.ViewSpecificPost',
        'app.Reactions',
        'app.Users',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('ViewSpecificPost') ? [] : ['className' => ViewSpecificPostTable::class];
        $this->ViewSpecificPost = $this->getTableLocator()->get('ViewSpecificPost', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->ViewSpecificPost);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\ViewSpecificPostTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\ViewSpecificPostTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
