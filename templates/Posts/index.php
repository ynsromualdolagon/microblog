<br>
  <?php
    echo $this->Form->create(null,['type' => 'get']);
    echo $this->Form->control('key',['label' => 'Search', 'value' => $this->request->getQuery('key'),
    'autocomplete' => 'off']);
    echo  $this->Form->submit(__('Search'),['class' => 'btn btn-primary']);
    echo $this->Form->end();
  ?>
  <?= $this->Html->link(__('New Post'), ['action' => 'add'], ['class' => 'button float-right']) ?>
<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
  Launch demo modal
</button> -->
  <!-- Modal -->
  <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add Comments</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="panel-body">
              <form action="javascript:void(0)" id="frm-add-comment" method="post">
                <div class="row custom-padding">
                <div class="col-sm-6">
                    <!-- text input -->
                    <div class="form-group">
                        <label>Comments:</label>
                        <input id="comment_id" type="text" required class="form-control" placeholder="add comments" name="comment">
                         <input value="<?= $identity->id ?>" id="user_id" type="text" required class="form-control" placeholder="add comments" name="user_id">
                          <input   id="idHolder" type="text" required class="form-control" placeholder="add comments" name="idHolder">
                    </div>
                </div>
                </div>
                <div class="row custom-padding">
                <div class="col-sm-6">
                    <!-- Select multiple-->
                    <div class="form-group">
                        <button id="submitBtn" class="btn btn-primary">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="off-canvas-wrapper">
      <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
        
        <div class="off-canvas-content" data-off-canvas-content>
         <?php foreach ($posts as $post): ?> 
          <?php if($post->deleted == null): ?>
            <div id="try" class="row medium-8 large-10 columns ">
              <div class="blog-post">
                <h3 style="text-transform: capitalize;"><?= $post->title ?> </h3>
              <strong>
                <?= $this->Html->link(__($post->user->fullname), ['controller' => 'profile','action' => 'profile', $post->user_id],['style' => 'text-transform: capitalize;']) ?>
              </strong>
              <p> <?= h($post->description) ?> </p>
              <?php if (h($post->file_name != '')): ?>
                <?php echo $this->Html->image(h($post->file_name), 
                  array(
                  "alt" => "logo",
                  'url' => array(
                             'controller' => 'Posts',
                             'action' => 'view', h($post->id)
                    )
                  )
                ); ?>
              <?php endif ?>
              </div>
          </div><br>
          <?php endif ?>
           <?php if($post->deleted != null): ?>
            <div id="try" class="row medium-8 large-7 columns ">
              <div class="blog-post">
                <h3 style="text-transform: capitalize;"><?= h($post->title) ?> <small><?= h($post->created) ?></small></h3>
              <strong>
                <?= $this->Html->link(__(h($post->user->fullname)), ['controller' => 'profile','action' => 'profile', $post->user_id],['style' => 'text-transform: capitalize;']) ?>
              </strong>
               <div class="card border-danger mb-3" style="max-width: 20rem;">
                <div class="card-header">Content Not found</div>
                <div class="card-body">
                  <p class="card-text">The page you requested cannot be displayed right now.</p>
                </div>
              </div>
              </div>
          </div><br>
          <?php endif ?>           
       <?php endforeach ?>
          <hr>
        </div>
      </div>
  </div>
<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="https://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.js"></script>
<script type="text/javascript">
  $(function(){
     $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': csrfToken 
        }
    });
    $('#submitBtn').click(function(){
      var comment = $("#comment_id").val();
      var post_id = $("#idHolder").val(); 
      $.ajax({
        method: "POST",
        type: "JSON",
        url: "<?= $this->Url->build(['controller' => 'Posts', 'action' => 'addComment']) ?>",
        data:{ 
          comment: comment
          ,post_id: post_id
        },
        success:function(response){
                location.reload();
            }
      });
    });
  });
</script>
