<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ReactionsFixture
 */
class ReactionsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'user_id' => 1,
                'post_id' => 1,
                'created' => '2022-01-06 14:50:02',
                'updated' => '2022-01-06 14:50:02',
                'deleted' => '2022-01-06 14:50:02',
            ],
        ];
        parent::init();
    }
}
