<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\ORM\TableRegistry;

/**
 * Posts Controller
 *
 * @property \App\Model\Table\PostsTable $Posts
 * @method \App\Model\Entity\Post[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CommentsController extends AppController
{
    public function initialize(): void
    {
        parent::initialize();
        $this->viewBuilder()->setLayout('app');
        $this->loadModel('Posts');
        $this->loadModel('Users');
        $this->loadModel('SharePosts');
        $this->loadModel('Likes');
        $this->loadModel('Comments');
        $this->loadModel('Followers');
    }

    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        $this->Authentication->addUnauthenticatedActions(['login']);
        $this->Authentication->addUnauthenticatedActions(['login', 'add']);
    }

    public function addComment()
    {
        $identity = $this->request->getAttribute('authentication')->getIdentity();
        $commentTable = $this->Comments;
        $comment = $this->Comments->newEmptyEntity();
        if ($this->request->is('ajax')) {
            $comment_here = $this->Comments->patchEntity($comment, $this->request->getData());
            $comment->user_id = $identity->id;
            if ($this->Comments->save($comment_here)) {
            }
        }

        return $this->redirect(['controller' => 'Posts', 'action' => 'view', $comment->post_id]);
        $this->set(compact('comment', 'users', 'identity'));
    }

    public function edit($id = null)
    {
        $identity = $this->request->getAttribute('authentication')->getIdentity();
        if ($this->request->is('post', 'ajax')) {
            $comments = $this->request->getData('comment');
            $post_id = $this->request->getData('post_id');

            $comments_table = TableRegistry::get('Comments');
            $comment = $comments_table->get($id);
            $comment->comment = $comments;
            $comment->post_id = $post_id;
            if ($comments_table->save($comment)) {
                $this->Flash->success(__('The comment has been updated.'));
            }
        }
        $this->set(compact('comment', 'users'));
    }

    public function delete($id = null)
    {
        $identity = $this->request->getAttribute('authentication')->getIdentity();
        $this->request->allowMethod(['post', 'delete']);
        $comments = $this->Comments->get($id);
        if ($comments->user_id = $identity->id) {
            $this->Comments->delete($comments);
            $this->Flash->success(__('The comment has been deleted.'));
        } else {
            $this->Flash->error(__('The comment could not be deleted. Please, try again.'));
        }

        return $this->redirect(['controller' => 'Posts', 'action' => 'view', $comments->post_id]);
    }
}
