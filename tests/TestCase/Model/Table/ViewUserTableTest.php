<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ViewUserTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ViewUserTable Test Case
 */
class ViewUserTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ViewUserTable
     */
    protected $ViewUser;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.ViewUser',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('ViewUser') ? [] : ['className' => ViewUserTable::class];
        $this->ViewUser = $this->getTableLocator()->get('ViewUser', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->ViewUser);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\ViewUserTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\ViewUserTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
