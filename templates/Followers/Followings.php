<br>
<?php
echo $this->Form->create(null, ['type' => 'get']);
echo $this->Form->control('key', ['label' => 'Search', 'value' => $this->request->getQuery('key'), 'class' => 'form-control me-sm-2']);
echo  $this->Form->submit(__('Search'), ['class' => 'btn btn-primary']);
echo $this->Form->end();
?>

<div class="off-canvas-wrapper">
  <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
    <div class="off-canvas-content" data-off-canvas-content>
      <div class="container mt-4 mb-4 p-3 d-flex justify-content-center">
      </div>
      <?php foreach ($following as $following) : ?>
        <div id="try" class="row medium-8 large-7 columns justify-content-center align-items-center">
          <div class="blog-post">
            <div class="card p-1">
              <div class=" image d-flex flex-column justify-content-center align-items-center"> <?php echo $this->Html->image((h($following->following->profile_pic)), array(
                                                                                                  'width' => '200px',
                                                                                                  'url' => array(
                                                                                                    'controller' => 'Profile',
                                                                                                    'action' => 'profile',h($following->following->id)
                                                                                                  )
                                                                                                )); ?>

                <span style="text-transform: capitalize;" class="name mt-3"><?= h($following->following->fullname) ?></span>
                <div class="d-flex flex-row justify-content-center align-items-center mt-3"><?= $this->Form->postLink(
                                                                                              __('Unfollow'),
                                                                                              ['action' => 'unfollow', h($following->id)],
                                                                                              ['confirm' => __('Are you sure you want to unfollow # {0}?', h($following->id)), 'class' => 'btn btn-primary']
                                                                                            ) ?> &nbsp&nbsp&nbsp</div>


              </div>
            </div>
          </div>
        </div>
        <br>
      <?php endforeach ?>
      <div class="paginator">
        <ul class="pagination">
          <?= $this->Paginator->numbers() ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
      </div>
    </div><br>
  </div>
  <hr>
</div>
</div>
</div>

<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="https://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.js"></script>
<script type="text/javascript">
  $(function() {
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': csrfToken
      }
    });
    $('#submitBtn').click(function() {
      var comment = $("#comment_id").val();
      var post_id = $("#idHolder").val();
      $.ajax({
        method: "POST",
        type: "JSON",
        url: "<?= $this->Url->build(['controller' => 'Posts', 'action' => 'addComment']) ?>",
        data: {
          comment: comment,
          post_id: post_id
        },
        success: function(response) {
          location.reload();
        }
      });
    });
    $('#followBtn').click(function() {
      var following = $('#user_id').val();
      $.ajax({
        method: "POST",
        url: "<?= $this->Url->build(['controller' => 'Posts', 'action' => 'following']) ?>",
        data: {
          following: following
        },
        success: function(response) {
          // location.reload();
        }
      });

    });
  });
</script>