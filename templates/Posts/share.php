<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Post $post
 * @var \Cake\Collection\CollectionInterface|string[] $users
 */
?>
<div class="row">
  
    <div class="column-responsive column-80">
          <?php foreach ($posts as $post): ?>
        <div class="posts form content">
            <?= $this->Form->create($post, ['type' => 'file']) ?>
            <p>  </p>
            <fieldset>
                <legend><?= __('Share Post') ?></legend>
                <div class="card border-primary mb-3" style="max-width: 20rem;">
                    <div style="text-transform: capitalize;" class="card-header">Post by: <?= $this->Html->link(__($post->fullname), ['action' => 'postprofile', $post->user_id],['style' => 'text-transform: capitalize;']) ?> </div>
                    <div class="card-body">
                        <h4 class="card-title">Description</h4>
                        <p class="card-text"><?= $post->description ?></p>
                        <?php echo $this->Html->image($post->file_name, 
                        array(
                        "alt" => "logo",
                         'url' => array(
                             'controller' => 'Posts',
                             'action' => 'view', $post->id
                            )
                          )
                        ); ?>
                    </div>
                </div>
            </fieldset>
           <?= $this->Form->submit(__('Share'),['action' => 'share','class' => 'btn btn-primary']); ?>
            <?= $this->Form->end() ?>
        </div>
         <?php endforeach ?>
    </div>
</div>
