<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Post $post
 * @var string[]|\Cake\Collection\CollectionInterface $users
 */
?>
<br>
<br>
<div class="row">
    <div class="column-responsive column-80">
        <div class="posts form content">
           <?= $this->Form->create($post, ['type' => 'file']) ?>
            <fieldset>
                <legend><?= __('Edit Post') ?></legend>
                <?php
                     echo $this->Html->image($post->file_name, 
                    array(
                    'class' => 'preview_img',
                    'width'=>'300px',
                    "alt" => "logo",
                    )
                  ); 
                    echo $this->Form->control('title');
                    echo $this->Form->label('description');
                    echo $this->Form->textarea('description');
                    echo $this->Form->control('image_file',
                        array(
                            'type'=>'file'
                            ,'default' => '200'
                            ,'onchange' => "javascript:imageURL(this);"
                        )
                    );
                   
                ?>
            </fieldset>
            <br>
            <?= $this->Form->submit(__('Update'),['class' => 'btn btn-primary']) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    function imageURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
              
                $('.preview_img').attr('src', e.target.result)
                 .width('300px')
                 .height('300px');
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>