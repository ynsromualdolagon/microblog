<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>

    <div class="column-responsive column-80">
        <div class="users form content">
            <?= $this->Form->create($user, ['type' => 'file']) ?>
            <fieldset>
                <legend><?= __('Edit Profile') ?></legend>
                <div class="form-group">
                    <?php
                    echo $this->Html->image(h($user->profile_pic), 
                    array(
                    'class' => 'preview_img',
                    'width'=>'300px',
                    "alt" => "logo",
                    )
                  ); 
                        echo $this->Form->control(__('fullname'),['class' => 'form-control']);
                        echo $this->Form->control('email');
                        echo $this->Form->control('password');
                        echo $this->Form->control('image_file',
                        array(
                            'type'=>'file'
                            ,'default' => '200'
                            ,'onchange' => "javascript:imageURL(this);"
                        )
                    );
                        
                    ?>
                </div>

            </fieldset>
            <?= $this->Form->submit(__('Submit'),['class' => 'btn btn-primary']) ?>
            <?= $this->Form->end() ?>
            <!-- <img  class="preview_img" id="preview_img" style="width:auto; height:auto;" /> -->
        </div>
    </div>
</div>
<script type="text/javascript">
    function imageURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
              
                $('.preview_img').attr('src', e.target.result)
                 .width('300px')
                 .height('300px');
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
