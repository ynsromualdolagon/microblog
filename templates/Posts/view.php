<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Post $post
 */
?>

<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Edit Comments</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="panel-body">
              <form action="javascript:void(0)" id="frm-add-comment" method="post">
                <div class="row custom-padding">
                <div class="col-sm-6">
                    <!-- text input -->
                    <div class="form-group">
                        <label>Comments:</label>
                        <input id="edit_comment_id" type="text" placeholder="edit comments">
                         <input hidden="" value="<?= $identity->id ?>" id="user_id" type="text" required class="form-control" placeholder="add comments" name="user_id">
                    </div>
                </div>
                </div>
                <div class="row custom-padding">
                <div class="col-sm-6">
                    <!-- Select multiple-->
                    <div class="form-group">
                        <button id="updateBtn" class="btn btn-primary">Update</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Add Comments</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="panel-body">
              <form action="javascript:void(0)" id="frm-add-comment" method="post">
                <div class="row custom-padding">
                <div class="col-sm-6">
                    <!-- text input -->
                    <div class="form-group">
                        <label>Comments:</label>
                        <input id="comment_id" type="text" required class="form-control" placeholder="add comments" name="comment">
                         <input hidden="" value="<?= $identity->id ?>" id="user_id" type="text" required class="form-control" placeholder="add comments" name="user_id">
                    </div>
                </div>
                </div>
                <div class="row custom-padding">
                <div class="col-sm-6">
                    <!-- Select multiple-->
                    <div class="form-group">
                        <button id="submitBtn" class="btn btn-primary">Submit</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
                </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
       <div class="row medium-8 large-7 columns">
          <?php foreach ($posts as $post): ?>
                <div class="blog-post">
                   <input hidden="" value="<?=$post->id ?>" id="idHolder" type="text" required class="form-control" placeholder="idHolder" name="idHolder">
                  <h3 style="text-transform: capitalize;"><?= $post->title ?></h3>
                   <?php  if ($identity->id == $post->user_id): ?>
                   <?= $this->Html->link(__('Edit'), ['action' => 'edit', $post->id]) ?>
                   <?= $this->Form->postLink(
                  __('Delete'),
                  ['action' => 'delete', $post->id],
                  ['confirm' => __('Are you sure you want to delete # {0}?', $post->id), 'class' => 'side-nav-item']
                  ) ?>
                <?php endif ?>
                <strong>
                <?= $this->Html->link(__(h($post->user->fullname)), ['controller' => 'profile','action' => 'profile', $post->user_id],['style' => 'text-transform: capitalize;']) ?>
                </strong>
                  <p hidden="" style="text-transform: capitalize;"> <?= $post->id ?> </p>
                  <p style="text-transform: capitalize;"> <?= h($post->description) ?> </p>
                  <?php if (h($post->file_name != '')) : ?>
                  <?php echo $this->Html->image(h($post->file_name), 
                    array(
                    "alt" => "logo",
                    'url' => array(
                               'controller' => 'Posts',
                               'action' => 'view', h($post->id)
                      )
                    )
                  ); ?>
                  <?php endif ?>
                  
                   <?php endforeach ?>
                    <div class="callout">
                    <ul class="menu simple">
                    <?php if(!empty($like)): ?>
                       <li><a data-id="<?= $post->id ?>" class="likeBtn"  id="likeBtn"href="javascript:void(0)">Like: </a></li>
                    <?php endif ?>
                    <?php foreach ($like as $like): ?>
                     <li><?= $this->Form->postLink(
                  __('Unlike'),
                  ['controller'=> 'Like','action' => 'unlike', h($like->id)],
                  ['confirm' => __('Are you sure you want to unlike # {0}?', $like->id), 'class' => 'side-nav-item']
                  ) ?></li>
                <?php endforeach ?>
                      <li><a data-id="<?= h($post->id) ?>" class="commentBtn"  id="commentBtn" data-toggle="modal" data-target="#exampleModal" href="javascript:void(0)">Comments: </a></li>
                    
                     <li><a data-id="<?= h($post->id) ?>" class="shareBtn"  id="shareBtn"href="javascript:void(0)">Share: </a></li>
                    </ul>
                    
                    <div class="card border-light mb-3" style="max-width: 20rem;">
                        <div class="card-header"></div>
                         <?php foreach ($comments as $comment): ?>
                        <div class="card-body" id="card-body">
                          <input hidden="" id="commentID" type="number" name="" value="<?= h($comment->id) ?>">
                          <strong>
                            <?= $this->Html->link(__($comment->user->fullname), [ 'controller' => 'Profile','action' => 'profile', h($comment->user_id)],['style' => 'text-transform: capitalize;']) ?>
                           </strong>
                           <p id="comment" class="card-text"><?= h($comment->comment) ?>
                           </p>
                            <?php  if ($identity->id == $comment->user_id): ?>
                           <a style="align-items: left" data-id="<?= h($comment->id) ?>" class="editBtn"  id="editBtn" data-toggle="modal" data-target="#editModal" href="javascript:void(0)">Edit: </a>
                           <?= $this->Form->postLink(
                    __('Delete'),
                    ['controller'=> 'Comments','action' => 'delete', $comment->id],
                    ['confirm' => __('Are you sure you want to delete # {0}?', $comment->id), 'class' => 'side-nav-item']
                    ) ?>
                         <?php endif ?>
                        </div>
                        <?php endforeach ?>
                    </div>
                </div>
            </div>
</div>
<script type="text/javascript">
  $(function(){
     $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': csrfToken 
        }
    });
    $('#submitBtn').click(function(){
      var comment = $("#comment_id").val();
      var post_id = $("#idHolder").val();
      $.ajax({
        method: "POST",
        type: "JSON",
        url: "<?= $this->Url->build(['controller' => 'Comments', 'action' => 'addComment']) ?>",
        data:{ 
          comment: comment
          ,post_id: post_id
        },
        success:function(response){
                location.reload();
            }
      });
    });
  });
  $('#likeBtn').click(function(){
     var post_id = $("#idHolder").val();
      $.ajax({
        method: "POST",
        type: "JSON",
        url: "<?= $this->Url->build(['controller' => 'Like', 'action' => 'like']) ?>",
        data:{ 
          post_id: post_id
        },
        success:function(response){
                location.reload();
            }
      });
  });
  
  $(document).ready(function()
    {
      $("#card-body").on('click','.editBtn',function()
      {
        var comment = $('#comment').text();
        $('#edit_comment_id').val(comment);

      })
  });

  $('#updateBtn').click(function(){
      var comment = $("#edit_comment_id").val().trim();
      var post_id = $("#idHolder").val();
      var commentID = $("#commentID").val();
      var comment_id = parseInt(commentID);
      $.ajax({
        method: "POST",
        type: "JSON",
        url: "<?= $this->Url->build(['controller' => 'Comments', 'action' => 'edit/comment_id'])?>",
        data:{
          comment: comment
          ,post_id: post_id
        },
        success:function(response){
              alert(url)
            }
      });
  });

    $('#shareBtn').click(function(){
     var post_id = $("#idHolder").val();
      $.ajax({
        method: "POST",
        type: "JSON",
        url: "<?= $this->Url->build(['controller' => 'Share', 'action' => 'share']) ?>",
        data:{ 
          post_id: post_id
        },
        success:function(response){
                location.reload();
            }
      });
  });
</script>