<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\VUsersTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\VUsersTable Test Case
 */
class VUsersTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\VUsersTable
     */
    protected $VUsers;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.VUsers',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('VUsers') ? [] : ['className' => VUsersTable::class];
        $this->VUsers = $this->getTableLocator()->get('VUsers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->VUsers);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\VUsersTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\VUsersTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
