<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>


 <br><br>

<center>
   <div class="card bg-light mb-3" style="max-width: 20rem;">
      <div class="card-header">Register</div>
      <div class="card-body">
            <h4 class="card-title"></h4>
            <?php echo $this->Flash->render();?>
            
  <?= $this->Form->create(($user))?>  
  <fieldset>
    <div class="form-group">
      
      <?php 

      if($errors) {
        foreach($errors as $error)
          echo '<font color="red">errors</font><br>';
      } else {
        echo "No errors.";
      }

      echo $this->Form->control('fullname', array(
        'label' => 'Please enter your fullname',
        'class' => 'pure-u-1-2',
        'autocomplete' => 'off',
        'required' => false  
      ));
     

        
      ?>
      
    </div>
    <div class="form-group">
    <?php  echo $this->Form->control('username', array(
        'label' => 'Please enter your username',
        'class' => 'pure-u-1-2',
        'autocomplete' => 'off',
        'required' => false
        
    )); ?>
    </div>
    <div class="form-group">
      <?php  echo $this->Form->control('email', array(
        'label' => 'Please enter your email',
        'class' => 'pure-u-1-2',
        'autocomplete' => 'off',
        'required' => false
        
    )); ?>
       <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
    </div>
    <div class="form-group">
    <?php  echo $this->Form->control('password', array(
        'label' => 'Please enter your password',
        'class' => 'pure-u-1-2',
        'autocomplete' => 'off',
        'required' => false
        
    )); ?>
    </div>

    <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary']) ?>
    <?= $this->Html->link("Login", ['action' => 'login']) ?>
  </fieldset>
 <?= $this->Form->end() ?>
      </div>
    </div> 
</center>

