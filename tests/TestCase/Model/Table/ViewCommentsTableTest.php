<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ViewCommentsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ViewCommentsTable Test Case
 */
class ViewCommentsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ViewCommentsTable
     */
    protected $ViewComments;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.ViewComments',
        'app.Users',
        'app.Posts',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('ViewComments') ? [] : ['className' => ViewCommentsTable::class];
        $this->ViewComments = $this->getTableLocator()->get('ViewComments', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->ViewComments);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\ViewCommentsTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\ViewCommentsTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
