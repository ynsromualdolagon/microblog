<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * SharePostsFixture
 */
class SharePostsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'share_id' => 1,
                'user_id' => 1,
                'post_id' => 1,
            ],
        ];
        parent::init();
    }
}
