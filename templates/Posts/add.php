<br>
<br>
<div class="row">
    <div class="column-responsive column-80">
        <div class="posts form content">
            <?= $this->Form->create($post, ['type' => 'file']) ?>
            <fieldset>
                <legend><?= __('Add Post') ?></legend>
              <?php
                    echo $this->Form->control('title',['required' => false]);
                    echo $this->Form->label('description');
                    echo $this->Form->textarea('description',['required' => false]);
                    echo $this->Form->control('image_file',
                        array(
                            'type'=>'file'
                            ,'default' => '200'
                            ,'onchange' => "javascript:imageURL(this);"
                        )
                    );
                ?>
            </fieldset>
            <?= $this->Form->submit(__('Submit'),['class' => 'btn btn-primary']) ?>
            <br>
            <?= $this->Form->end() ?>
            <img  class="preview_img" id="preview_img" style="width:auto; height:auto;" />
        </div>
    </div>

</div>
<script type="text/javascript">
    function imageURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
              
                $('#preview_img').attr('src', e.target.result)
                 .width('300px')
                 .height('300px');
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>