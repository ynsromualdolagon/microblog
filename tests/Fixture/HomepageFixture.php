<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * HomepageFixture
 */
class HomepageFixture extends TestFixture
{
    /**
     * Table name
     *
     * @var string
     */
    public $table = 'homepage';
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'fullname' => 'Lorem ipsum dolor sit amet',
                'following_name' => 'Lorem ipsum dolor sit amet',
                'title' => 'Lorem ipsum dolor sit amet',
                'profile_pic' => 'Lorem ipsum dolor sit amet',
                'email' => 'Lorem ipsum dolor sit amet',
                'id' => 1,
                'user_id' => 1,
                'description' => 'Lorem ipsum dolor sit amet',
                'file_name' => 'Lorem ipsum dolor sit amet',
                'created' => '2022-01-12 03:29:13',
                'modified' => '2022-01-12 03:29:13',
                'f_user_id' => 1,
                'following_user_id' => 1,
                'uid' => 1,
            ],
        ];
        parent::init();
    }
}
