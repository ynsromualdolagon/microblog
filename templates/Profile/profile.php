<br>
  <?php
    echo $this->Form->create(null,['type' => 'get']);
    echo $this->Form->control('key',['label' => 'Search', 'value' => $this->request->getQuery('key'),'class' => 'form-control me-sm-2']);
    echo  $this->Form->submit(__('Search'),['class' => 'btn btn-primary']);
    echo $this->Form->end();
  ?>
  <?= $this->Html->link(__('New Post'), [ 'controller' => 'Posts','action' => 'add'], ['class' => 'button float-right']) ?>
  <div class="off-canvas-wrapper">
      <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
        <div class="off-canvas-content" data-off-canvas-content>
        <div class="container mt-4 mb-4 p-3 d-flex justify-content-center"> 
           <?php foreach ($user as $user): ?>
            <div class="card p-4" id="card-body">
              <?php if ($identity->id == $user->id): ?>
                <div class=" image d-flex flex-column justify-content-center align-items-center">
                <?php echo $this->Html->image(h($user->profile_pic), array(
                	'width'=>'200px',
                	'url' => array(
                             'controller' => 'Profile',
                             'action' => 'profile', h($user->id)
                    )            	
                ));?>
                 <span id="fullname" style="text-transform: capitalize;" class="name mt-3"><?= h($user->fullname) ?></span>
                    <div class="d-flex flex-row justify-content-center align-items-center mt-3"><?= $this->Html->link(__('Followers'), ['controller' => 'Followers','action' => 'listfollower' , $user->id], ['class' => 'btn btn-primary']) ?> 
                    &nbsp&nbsp&nbsp 
                    <?= $this->Html->link(__('Following'), ['controller' => 'Followers','action' => 'followings' , h($user->id)], ['class' => 'btn btn-primary']) ?></div>
                    <div class=" d-flex mt-2">
                  		<?php echo $this->Html->link('Edit Profile'
                  			,array('controller' => 'Users', 'action'=>'edit',h( $user->id))
                   			,array('class' => 'btn1 btn-dark')); 
                   		?>
                  	</div>
                   

                    <div class=" px-2 rounded mt-4 date "> <span class="join">Joined <?= h($user->created) ?></span> </div>
                </div>
              <?php endif ?>
              <?php  if ($identity->id != $user->id): ?>
                <input hidden="" value="<?= h($user->id) ?>" id="user_id" type="text" required class="form-control" placeholder="idHolder" name="user_id">
                <div class=" image d-flex flex-column justify-content-center align-items-center"><?php echo $this->Html->image(h($user->profile_pic), array(
                  'width'=>'200px',
                  'url' => array(
                             'controller' => 'Profile',
                             'action' => 'profile', h($user->id)
                    )             
                ));?>

                 <span style="text-transform: capitalize;" class="name mt-3"><?= h($user->fullname) ?></span>
                   <div class="d-flex flex-row justify-content-center align-items-center mt-3">
                     <button id="followBtn" type="button" class="btn btn-primary">Follow</button>
                   </div>
                    <div class=" px-2 rounded mt-4 date "> <span class="join">Joined <?= h($user->created) ?></span> </div>
                </div>
              <?php endif ?>
                </div>
                 <?php endforeach ?>       
            </div>
             <?php foreach ($posts as $post): ?>      
            <div id="try" class="row medium-8 large-7 columns ">
              <div class="blog-post">
                <h3 style="text-transform: capitalize;"><?= h($post->title) ?> </h3>
              <strong>
                <?= $this->Html->link(__(h($post->user->fullname)), ['controller' => 'profile','action' => 'profile', h($post->user_id)],['style' => 'text-transform: capitalize;']) ?>
              </strong>
                <p> <?= h($post->description) ?> </p>
              <?php if (h($post->file_name) != '') : ?>
                <?php echo $this->Html->image(h($post->file_name), 
                  array(
                  "alt" => "logo",
                  'url' => array(
                             'controller' => 'Posts',
                             'action' => 'view', h($post->id)
                    )
                  )
                ); ?>
              </div>
          </div><br>
          <?php endif ?>
       <?php endforeach ?>
       <?php foreach ($sharePosts as $share): ?>
        <?php if($share->post->deleted != null): ?>
         <div id="try" class="row medium-8 large-7 columns ">
              <div class="blog-post">
               <div class="card border-danger mb-3" style="max-width: 20rem;">
                <div class="card-header">Content Not found</div>
                <div class="card-body">
                  <p class="card-text">The page you requested cannot be displayed right now.</p>
                </div>
              </div>
              </div>
          </div><br>
        <?php endif ?>
        <?php if($share->post->deleted == null): ?>
             <div id="try" class="row medium-8 large-7 columns ">
              <div class="blog-post">
              <div class="card border-primary mb-3" style="max-width: 20rem;">
                    <div class="card-header">Your share this posts</div>
                    <div class="card-body">
                      <h4 class="card-title"><?= $this->Html->link(__(h($share->user->fullname)), ['controller' => 'profile','action' => 'profile', $share->user_id],['style' => 'text-transform: capitalize;']) ?></h4>
                        <h3 style="text-transform: capitalize;"><?= h($share->post->title) ?> </h3>
                      <p class="card-text"><?= h($share->post->description) ?></p>
                      <?php if ($share->post->file_name != '') : ?>
                            <?php echo $this->Html->image(h($share->post->file_name), 
                              array(
                              "alt" => "logo",
                              'url' => array(
                                         'controller' => 'Posts',
                                         'action' => 'view', h($share->post->id)
                                )
                              )
                            ); ?>
                          <?php endif ?>
                    </div>
              </div>
              </div>
</div><br>
        <?php endif ?>
       <?php endforeach ?>
        </div>
          <hr>
        </div>
      </div>
  </div>
<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="https://dhbhdrzi4tiry.cloudfront.net/cdn/sites/foundation.js"></script>
<script type="text/javascript">
$(function(){
  $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': csrfToken 
      }
  });
 
  
   $('#followBtn').click(function(){
    var following  = $('#user_id').val();
    $.ajax({
        method: "POST",
        url: "<?= $this->Url->build(['controller' => 'followers', 'action' => 'add']) ?>",
        data:{ 
         following: following
        },
        success:function(response){
                location.reload();
            }
      });

  });
});

</script>
