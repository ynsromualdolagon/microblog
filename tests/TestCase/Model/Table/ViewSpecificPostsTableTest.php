<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ViewSpecificPostsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ViewSpecificPostsTable Test Case
 */
class ViewSpecificPostsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ViewSpecificPostsTable
     */
    protected $ViewSpecificPosts;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.ViewSpecificPosts',
        'app.Users',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('ViewSpecificPosts') ? [] : ['className' => ViewSpecificPostsTable::class];
        $this->ViewSpecificPosts = $this->getTableLocator()->get('ViewSpecificPosts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->ViewSpecificPosts);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\ViewSpecificPostsTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\ViewSpecificPostsTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
