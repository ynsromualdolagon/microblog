<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\ORM\TableRegistry;

/**
 * Posts Controller
 *
 * @property \App\Model\Table\PostsTable $Posts
 * @method \App\Model\Entity\Post[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ProfileController extends AppController
{
    public function initialize(): void
    {
        parent::initialize();
        $this->viewBuilder()->setLayout('app');
        $this->loadModel('Posts');
        $this->loadModel('Users');
        $this->loadModel('SharePosts');
        $this->loadModel('Likes');
        $this->loadModel('Comments');
        $this->loadModel('Followers');
    }

    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        $this->Authentication->addUnauthenticatedActions(['login']);
        $this->Authentication->addUnauthenticatedActions(['login', 'add']);
    }

    public function profile($user_id = null)
    {
        $identity = $this->request->getAttribute('authentication')->getIdentity();
        $users = TableRegistry::get('Users');
        $user = $users->find()
            ->where(['id' => $user_id])
            ->all();
        $key = $this->request->getQuery('key');
        if ($key) {
            $posts = $this->Posts
            ->find('all')
            ->contain(['Users'])
            ->where(['fullname like' => '%' . $key . '%'])
            ->all();
        } else {
             $posts = $this->Posts->find('all')
                ->contain(['Users'])
                ->where(['user_id' => $user_id])
                ->order(['Posts.id' => 'desc'])
                ->all();
        }
         $sharePosts = $this->SharePosts->find('all', ['withDeleted'])
                ->contain(['Posts'])
                ->contain(['Users'])
                ->where(['SharePosts.user_id' => $user_id])
                ->all();
        $this->set(compact('user', 'posts', 'identity', 'sharePosts'));
    }
}
