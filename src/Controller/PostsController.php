<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Posts Controller
 *
 * @property \App\Model\Table\PostsTable $Posts
 * @method \App\Model\Entity\Post[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PostsController extends AppController
{
    public function initialize(): void
    {
        parent::initialize();
        $this->viewBuilder()->setLayout('app');
        $this->loadModel('Posts');
        $this->loadModel('Users');
        $this->loadModel('SharePosts');
        $this->loadModel('Likes');
        $this->loadModel('Comments');
        $this->loadModel('Followers');
    }

    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        $this->Authentication->addUnauthenticatedActions(['login']);
        $this->Authentication->addUnauthenticatedActions(['login', 'add']);
    }

    public function index()
    {
        $key = $this->request->getQuery('key');
        $identity = $this->request->getAttribute('authentication')->getIdentity();
        if ($key) {
            $posts = $this->Posts
                ->find('all')
                ->contain(['Users'])
                ->where(['fullname like' => '%' . $key . '%'])
                ->all();
        } else {
            $posts = $this->Posts->find('all')
                ->innerJoin(
                    ['Followers' => 'followers'],
                    [
                        'OR' =>
                        [
                            'Posts.user_id' => $identity->id,
                            [
                                'Followers.following_user_id = Posts.user_id',
                                'Followers.follower_user_id' => $identity->id,
                            ],
                        ],
                    ]
                )
                ->distinct(['Posts.id'])
                ->contain('Users')
                ->order(['Rand()'])
                ->all();
            // dd($posts->toArray());
        }
        $this->set(compact('posts', 'identity'));
    }

    public function add()
    {

        $identity = $this->request->getAttribute('authentication')->getIdentity();
        $post = $this->Posts->newEmptyEntity();
        if ($this->request->is('post')) {
            $post = $this->Posts->patchEntity($post, $this->request->getData());
            $post->user_id = $identity['id'];
            if (!$post->getErrors) {
                $image = $this->request->getData('image_file');
                $name = $image->getClientFilename();
                $targetPath = WWW_ROOT . 'img' . DS . $name;

                if ($name) {
                    $image->moveTo($targetPath);
                }
                $post->file_name = $name;
            }
            if ($this->Posts->save($post)) {
                $this->Flash->success(__('The post has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The post could not be saved. Please, try again.'));
        }
        $users = $this->Posts->Users->find('list', ['limit' => 200])->all();
        $this->set(compact('post', 'users', 'identity'));
    }

    public function view($id = null)
    {
        $identity = $this->request->getAttribute('authentication')->getIdentity();
        $posts = $this->Posts
            ->find('all')
            ->contain(['Users'])
            ->contain(['Likes'])
            ->where(['Posts.id' => $id])
            ->all();
        if ($this->request->is(['patch', 'post', 'put'])) {
            $post = $this->Posts->patchEntity($posts, $this->request->getData());
            if ($this->Posts->save($post)) {
                $this->Flash->success(__('The post has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The post could not be saved. Please, try again.'));
        }
        $comments = $this->Comments
            ->find('all')
            ->contain(['Users'])
            ->contain(['Posts'])
            ->where(['Posts.id' => $id])
            ->all();
        $like = $this->Likes->find('all')
            ->where(['Likes.user_id' => $identity->id]);
        $this->set(compact('posts', 'identity', 'comments', 'like'));
    }

    public function edit($id = null)
    {
        $identity = $this->request->getAttribute('authentication')->getIdentity();

        $post = $this->Posts->get($id, [
            'contain' => ['Users'],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $post = $this->Posts->patchEntity($post, $this->request->getData());
            if (!$post->getErrors) {
                $image = $this->request->getData('image_file');
                $name = $image->getClientFilename();
                $targetPath = WWW_ROOT . 'img' . DS . $name;

                if ($name) {
                    $image->moveTo($targetPath);
                }
                $post->file_name = $name;
            }
            if ($post->user->id = $identity->id) {
                $this->Posts->save($post);
                $this->Flash->success(__('The post has been saved.'));

                return $this->redirect(['action' => 'view', $id]);
            }
            $this->Flash->error(__('The post could not be saved. Please, try again.'));
        }
        $users = $this->Posts->Users->find('list', ['limit' => 200])->all();
        $this->set(compact('post', 'identity'));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $post = $this->Posts->get($id);
        if ($this->Posts->delete($post)) {
            $this->Flash->success(__('The post has been deleted.'));
        } else {
            $this->Flash->error(__('The post could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
