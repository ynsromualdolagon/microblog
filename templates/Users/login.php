<br><br>
<center>
   <div class="card bg-light mb-3" style="max-width: 20rem;">
      <div class="card-header">Please enter your username and password</div>
      <div class="card-body">
            <h4 class="card-title"></h4>
            <div class="form-group">
                <?= $this->Flash->render() ?>
                <?= $this->Form->create() ?>
                <fieldset>
                    <div class="form-group">
                      <?php echo $this->Form->control(__('email'),array(
                        'class' => 'form-control'
                        ,'autocomplete' => 'off'
                        )); 
                      ?>
                    </div>
                     <div class="form-group">
                      <?php echo $this->Form->control(__('password'),['class' => 'form-control']); ?>
                    </div>
                </fieldset>
                <?= $this->Form->submit(__('Login'),['Controller' => 'Login','action' => 'login','class' => 'btn btn-primary']); ?>
                <?= $this->Form->end() ?>
                <?= $this->Html->link("Register", ['action' => 'add']) ?>
        </div>
      </div>
    </div> 
</center>
