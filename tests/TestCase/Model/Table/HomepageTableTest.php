<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\HomepageTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\HomepageTable Test Case
 */
class HomepageTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\HomepageTable
     */
    protected $Homepage;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Homepage',
        'app.Users',
        'app.FUsers',
        'app.FollowingUsers',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Homepage') ? [] : ['className' => HomepageTable::class];
        $this->Homepage = $this->getTableLocator()->get('Homepage', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Homepage);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\HomepageTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\HomepageTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
