<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DebugFollowersTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DebugFollowersTable Test Case
 */
class DebugFollowersTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\DebugFollowersTable
     */
    protected $DebugFollowers;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.DebugFollowers',
        'app.Posts',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('DebugFollowers') ? [] : ['className' => DebugFollowersTable::class];
        $this->DebugFollowers = $this->getTableLocator()->get('DebugFollowers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->DebugFollowers);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\DebugFollowersTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\DebugFollowersTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
