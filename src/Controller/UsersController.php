<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Mailer\Mailer;
use Cake\Mailer\MailerAwareTrait;
use Cake\Routing\Router;
use Cake\Utility\Text;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    use MailerAwareTrait;

    public function initialize(): void
    {
        parent::initialize();
        $this->viewBuilder()->setLayout('app');
    }

    public function index()
    {
        $query = $this->Users;
        $users = $this->paginate($query);
        $this->set(compact('users'));
    }

    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        $this->Authentication->addUnauthenticatedActions(['login']);
        $this->Authentication->addUnauthenticatedActions(['login', 'add', 'activate']);
    }

    public function login()
    {
        $identity = $this->request->getAttribute('authentication')->getIdentity();
        $this->request->allowMethod(['get', 'post']);
        $result = $this->Authentication->getResult();

        $users = $this->getTableLocator()->get('Users');

        if ($result->isValid()) {
            $query = $users
                ->find()
                ->where(['id =' => $identity['id']])
                ->all();
            foreach ($query as $query) {
            }
            if (!empty($query->token)) {
                $this->Authentication->logout();
                $this->Flash->error(__('Please activate this account'));
            } else {
                $redirect = $this->request->getQuery('redirect', [
                    'controller' => 'posts',
                    'action' => 'index',
                ]);

                return $this->redirect($redirect);
            }
        }
        if ($this->request->is('post') && !$result->isValid()) {
            $this->Flash->error(__('Invalid username or password'));
        }
    }

    public function logout()
    {
        $result = $this->Authentication->getResult();
        if ($result->isValid()) {
            $this->Authentication->logout();

            return $this->redirect(['controller' => 'Users', 'action' => 'login']);
        }
    }

    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => [],
        ]);

        $this->set(compact('user'));
    }

    public function add()
    {

        $user = $this->Users->newEmptyEntity();
        $errors = [];
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            $user->token = Text::uuid();
            $user->profile_pic = 'profile_pic.png';
            if ($user->getErrors()) {
                // dd($user->getErrors());
                $this->Flash->error(__('Unable to register you.  Please make sure you have completed all fields correctly.'));
                $errors = $user->getErrors();

                $this->set(compact('user', 'errors'));

                return $this->redirect(['action' => 'add']);
            } else {
                $this->Users->save($user);
                $mailer = new Mailer('default');
                $mailer->setFrom(['me@example.com' => 'Activate Account For Microblog'])
                    ->setTo($user->email)
                    ->setSubject('User Activation')
                    ->deliver(Router::url(['controller' => 'Users', 'action' => 'activate', $user->id, $user->token], true));
                $this->Flash->error(__('Please make sure to activate your account'));

                return $this->redirect(['action' => 'login']);
            }
        }

        $this->set(compact('user', 'errors'));
    }

    public function activate($id, $token)
    {
        $usersTable = $this->getTableLocator()->get('Users');
        $users = $usersTable->get($id);
        if ($users->id == $id && $users->token == $token) {
            $users->token = null;
            $usersTable->save($users);
            $this->Flash->success(__('Account Activated.'));

            return $this->redirect(['action' => 'login']);
        } elseif ($users->id == $id && $users->token != $token) {
            $this->Flash->error(__('Unknown Account.'));
        }
    }

    public function edit($id = null)
    {
        $identity = $this->request->getAttribute('authentication')->getIdentity();
        $user = $this->Users->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if (!$user->getErrors) {
                $image = $this->request->getData('image_file');
                $name = $image->getClientFilename();
                $targetPath = WWW_ROOT . 'img' . DS . $name;

                if ($name) {
                    $image->moveTo($targetPath);
                }
                $user->profile_pic = $name;
            }
            if ($user->id = $identity->id) {
                $this->Users->save($user);
                $this->Flash->success(__('Profile Updated.'));

                return $this->redirect(['controller' => 'Profile', 'action' => 'profile', $user->id]);
            }
            $this->Flash->error(__('The profile could not be saved. Please, try again.'));
        }
        $this->set(compact('user', 'identity'));
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
