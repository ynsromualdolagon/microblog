<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ViewUserFixture
 */
class ViewUserFixture extends TestFixture
{
    /**
     * Table name
     *
     * @var string
     */
    public $table = 'view_user';
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'email' => 'Lorem ipsum dolor sit amet',
                'password' => 'Lorem ipsum dolor sit amet',
                'created' => '2021-12-27 06:13:48',
                'modified' => '2021-12-27 06:13:48',
                'fullname' => 'Lorem ipsum dolor sit amet',
                'username' => 'Lorem ipsum dolor sit amet',
                'deleted' => '2021-12-27 06:13:48',
                'activate_user' => 1,
                'token' => 'Lorem ipsum dolor sit amet',
            ],
        ];
        parent::init();
    }
}
