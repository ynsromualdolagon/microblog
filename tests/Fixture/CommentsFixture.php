<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * CommentsFixture
 */
class CommentsFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'post_id' => 1,
                'user_id' => 1,
                'created' => '2022-01-04 01:37:19',
                'updated' => '2022-01-04 01:37:19',
                'deleted' => '2022-01-04 01:37:19',
            ],
        ];
        parent::init();
    }
}
