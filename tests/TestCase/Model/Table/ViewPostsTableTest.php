<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ViewPostsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ViewPostsTable Test Case
 */
class ViewPostsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ViewPostsTable
     */
    protected $ViewPosts;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.ViewPosts',
        'app.Users',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('ViewPosts') ? [] : ['className' => ViewPostsTable::class];
        $this->ViewPosts = $this->getTableLocator()->get('ViewPosts', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->ViewPosts);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\ViewPostsTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\ViewPostsTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
