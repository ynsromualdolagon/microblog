<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FollowingListTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FollowingListTable Test Case
 */
class FollowingListTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\FollowingListTable
     */
    protected $FollowingList;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.FollowingList',
        'app.Users',
        'app.FollowingUsers',
        'app.FollowerUsers',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('FollowingList') ? [] : ['className' => FollowingListTable::class];
        $this->FollowingList = $this->getTableLocator()->get('FollowingList', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->FollowingList);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\FollowingListTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     * @uses \App\Model\Table\FollowingListTable::buildRules()
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
